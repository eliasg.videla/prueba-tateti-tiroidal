import java.util.HashMap;
/*
 * En esta clase se guardan todas las variantes ganadoras para cada cuadrante,
 * est�n las variantes horizontales, verticales y diagonales/toroidal
 * 
 * Parece todo muy manual y hardcodeado, se puede mejorar pero igual esto se ejecuta una sola
 * vez y es todo constante (verificar pero estoy seguro que si)
 * 
 * */
public class Variantes {
	
/*
 * Estas cuatro estructuras de datos son las que van a ser luego consultadas en las otras clases
 * 
 * */	
	private HashMap<Integer, int[]> horizontales   = new HashMap<Integer,int[]>();
	private HashMap<Integer, int[]> verticales     = new HashMap<Integer,int[]>();
	private HashMap<Integer, int[]> diagonalesA    = new HashMap<Integer,int[]>();
	private HashMap<Integer, int[]> diagonalesB    = new HashMap<Integer,int[]>();
	
	/*
	 * Guardo en 9 diferentes arreglos las variantes para cada orientacion (esto es solamente para que
	 * se pueda guardar luego en el hashmap)
	 * 
	 * */

	int [] horizontal012 = {0,1,2} ;
	int [] horizontal345 = {3,4,5} ;
	int [] horizontal678 = {6,7,8} ;

	int [] vertical036   = {0,3,6} ;
	int [] vertical147   = {1,4,7} ;
	int [] vertical258   = {2,5,8} ;
	
	int [] diagonalA_048 = {0,4,8} ;
	int [] diagonalA_156 = {1,5,6} ;
	int [] diagonalA_237 = {2,3,7} ;
	
	int [] diagonalB_057 = {0,5,7} ;
	int [] diagonalB_138 = {1,3,8} ;
	int [] diagonalB_246 = {2,4,6} ;

	Variantes()
	{
		/*
		 * Guardo los arreglos en el hashmap respetando la variante ganadora de cada cuadrante
		 * 
		 * */
		
		horizontales.put(0, horizontal012);
		horizontales.put(1, horizontal012);
		horizontales.put(2, horizontal012);
		horizontales.put(3, horizontal345);
		horizontales.put(4, horizontal345);
		horizontales.put(5, horizontal345);
		horizontales.put(6, horizontal678);
		horizontales.put(7, horizontal678);
		horizontales.put(8, horizontal678);
		
		verticales.put(0, vertical036);
		verticales.put(1, vertical147);
		verticales.put(2, vertical258);
		verticales.put(3, vertical036);
		verticales.put(4, vertical147);
		verticales.put(5, vertical258);
		verticales.put(6, vertical036);
		verticales.put(7, vertical147);
		verticales.put(8, vertical258);
		
		diagonalesA.put(0, diagonalA_048);
		diagonalesA.put(1, diagonalA_156);
		diagonalesA.put(2, diagonalA_237);
		diagonalesA.put(3, diagonalA_237);
		diagonalesA.put(4, diagonalA_048);
		diagonalesA.put(5, diagonalA_156);
		diagonalesA.put(6, diagonalA_156);
		diagonalesA.put(7, diagonalA_237);
		diagonalesA.put(8, diagonalA_048);
		
		diagonalesB.put(0, diagonalB_057);
		diagonalesB.put(1, diagonalB_138);
		diagonalesB.put(2, diagonalB_246);
		diagonalesB.put(3, diagonalB_138);
		diagonalesB.put(4, diagonalB_246);
		diagonalesB.put(5, diagonalB_057);
		diagonalesB.put(6, diagonalB_246);
		diagonalesB.put(7, diagonalB_057);
		diagonalesB.put(8, diagonalB_138);
	}
	
	
	public int[] getHorizontales(int ubicacion) 
	{
		return horizontales.get(ubicacion);
	}
	
	public int[] getVerticales(int ubicacion) 
	{
		return verticales.get(ubicacion);
	}

	public int[] getDiagonalesA(int ubicacion) 
	{
		return diagonalesA.get(ubicacion);
	}

	public int[] getDiagonalesB(int ubicacion) 
	{
		return diagonalesB.get(ubicacion);
	}
}

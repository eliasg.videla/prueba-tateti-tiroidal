
public class Tablero {
	
	/*
	 * En esta clase se maneja el estado del tablero
	 * 
	 * */
	private String[] _tablero = { "VACIO", "VACIO", "VACIO", 
								  "VACIO", "VACIO", "VACIO", 
								  "VACIO", "VACIO", "VACIO" };
	
	public String verEstado(int indice) 
	{
		return _tablero[indice];
	}
	
	public void cambiarEstado(int indice, int numJugador) 
	{
		//verificar que el estado no este vacio
		if(numJugador == 1)
			_tablero[indice] = "CRUZ";
		if(numJugador == 2)
			_tablero[indice] = "CIRCULO";
	}
	
	
	public String[] verTablero() 
	{
		return _tablero;
	}
	
	//funcion solo para usarse en el main, borrarlo despues
	public void imprimir() 
	{
		for(int i = 0; i < _tablero.length; i++) {
			if(i == 3 || i == 6 )
				System.out.println("");
			System.out.print(_tablero[i] + " ");
			}
	}

}

import java.util.Scanner;

public class Juego {
	/*
	 * Estas funciones son inutiles y solo sirven para usarse en el main
	 * 
	 * */
	public static void pedirQueIngreseNumero(int numJugador) 
	{
		if(numJugador == 1)
			System.out.println("Turno de la cruz, ingresa numero de cuadro ");
		if (numJugador == 2)
			System.out.println("Turno de el circulo, ingresa numero de cuadro ");
	}
	
	public static void imprimirGanador(int numJugador) 
	{
		if(numJugador == 2)
			System.out.println(">>>Gano CRUZ<<<");
		else
			System.out.println(">>>Gano CIRCULO<<<");
	}
	
	
	/*
	 * El main simula la clase interfaz
	 * 
	 * */
	public static void main(String[] args) 
	{
		Tablero tablero = new Tablero();
		Variantes variante = new Variantes();
		Scanner entrada = new Scanner(System.in);
		int jugada = 0;
		int turno = 1;
		boolean alguienGano = false;
		
		while( jugada < 9 && !alguienGano) 
		{
			tablero.imprimir();
			System.out.println("");
			pedirQueIngreseNumero(turno);
			int ubicacion = entrada.nextInt();
			tablero.cambiarEstado(ubicacion, turno);
			if(jugada >= 4 && Control.hayGanador(ubicacion, variante, tablero)) {
				alguienGano = true;
				}
			
			if(turno == 1)
				turno = 2;
			else
				turno = 1;
			
			jugada++;
			
		}
		System.out.println("-------");
		tablero.imprimir();
		System.out.println("");
		System.out.println("-------");
		imprimirGanador(turno);
	}

}

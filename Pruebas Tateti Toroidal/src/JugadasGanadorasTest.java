import static org.junit.Assert.*;

import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class JugadasGanadorasTest {

	Tablero tablero;
	Variantes variante;
	
	@Before
	public void inicializacion() 
	{
		tablero = new Tablero();
		variante = new Variantes();
	}
	
	//Primero pruebas con la cruz---------------------------------------------------------
	
	@Test
	public void cruzJugadaHorizontalTest() 
	{
		//tablero.cambiarEstado(ubicacion, turno);
		tablero.cambiarEstado(7, 1);  
		tablero.cambiarEstado(1, 2);  
		tablero.cambiarEstado(8, 1);  
		tablero.cambiarEstado(4, 2);  
		
		
		assertTrue(Control.hayGanador(6, variante, tablero));

	}

}
